import React from 'react';
import ReactDOM from 'react-dom';
import WeatherUI from './weather/weather-ui';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<WeatherUI />, div);
  ReactDOM.unmountComponentAtNode(div);
});
