import React, { Component } from 'react';
import './styles.css';
import Fetcher from './weather-data-fetch';
import WeatherWidget from './weather-widget';
import { Grid, Segment, Divider } from 'semantic-ui-react';

class WeatherOtherCities extends Component {

    constructor(props) {
        super(props);
        this.state = { cities: [], weather: [] }
    }

    componentWillReceiveProps(nextProps) {
        //store async return
        this.setState({ cities: nextProps.cities }, () => {
            var cities = this.state.cities.slice();
            //fetch data from service
            cities.map((city) => {
                Fetcher(city.CityCode).then(response => this.setState({ weather: this.state.weather.concat(response) }));
            });
        })
    }

    render() {
        return (
            <Segment raised className="other-cities">
                <Grid stackable doubling columns={4}>
                    <Grid.Row>
                        {this.state.weather.map((weather, key) => {
                            return <Grid.Column key={key}>
                                <div className="grid-column">
                                    <WeatherWidget
                                        temp={weather.temp}
                                        description={weather.desc}
                                        cityID={weather.cityID}
                                        cityName={weather.cityName} />
                                </div>
                                <br /><br />
                            </Grid.Column>;
                        })}
                    </Grid.Row>
                </Grid>
            </Segment>
        );
    }
}

export default WeatherOtherCities;