import React from 'react';
import { render } from 'react-dom';

const SERVER = 'http://api.openweathermap.org/';
const API_URL = 'data/2.5/weather?units=metric&appid=';
const API_KEY = '5ce055e06ec5fc42acd3e579abfc0c74';
const ATTR = '&id=';
const serverWithApiUrl = SERVER + API_URL + API_KEY + ATTR;


    function Fetcher(cityID){
        return fetch(serverWithApiUrl + cityID)
        .then(response => response.json())
        .then(data => {
            var weather = {temp:"", desc:"", cityID:"", cityName:""};
            weather.temp = data.main.temp;
            weather.desc = data.weather[0].description;
            weather.cityID = cityID;
            weather.cityName = data.name;
            return weather;
        });
    }

export default Fetcher;