import React, { Component } from 'react';
import './styles.css';
import Fetcher from './weather-data-fetch';
import WeatherWidget from './weather-widget';
import { Dropdown } from 'semantic-ui-react';
import { Divider } from 'semantic-ui-react';
import { Label, Segment } from 'semantic-ui-react'


class WeatherMainWidget extends Component {

    cities = [];

    constructor(props) {
        super(props);
        this.state = { weather: { temp: "", desc: "", cityID: "", cityName: "" }, currentCity: "", cityOptions: [] }
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event, { value }) {
        //store async return
        this.setState({ currentCity: value }, () => {
            Fetcher(this.state.currentCity).then(response => this.setState({ weather: response }));
        })
    }

    componentDidMount() {
        this.cities = this.props.cities;
    }

    componentWillReceiveProps(nextProps) {
        this.cities = nextProps.cities;
        var cityOptions = [];

        if (this.cities[0]) {
            Fetcher(this.cities[0].CityCode).then(response => this.setState({ weather: response }));
        }
        this.cities.map((city, i) => {
            cityOptions = cityOptions.concat({ key: i, value: city.CityCode, text: city.CityName });
        })
        this.setState({cityOptions: cityOptions});
    }

    getInitCityName(){
        if(this.state.cityOptions[0]){
            return this.state.cityOptions[0].text;
        } else{
            return "City";
        }
    }

    render() {
        return (
            <div>
                <span>
                    Show me Weather for,&nbsp;&nbsp;
                <Dropdown className="dropdown" placeholder={this.getInitCityName()} onChange={this.handleChange} inline options={this.state.cityOptions} />
                </span><br /><Divider />
                <Segment raised className="detail-pane">
                <Label attached='top'>Instant Weather</Label>
                    <WeatherWidget
                        temp={this.state.weather.temp}
                        description={this.state.weather.desc}
                        cityID={this.state.weather.cityID}
                        cityName={this.state.weather.cityName} />
                </Segment>
            </div>
        );
    }
}

export default WeatherMainWidget;
