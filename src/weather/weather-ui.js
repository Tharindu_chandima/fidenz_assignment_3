import React, { Component } from 'react';
import './styles.css';
import WeatherMainWidget from './weather-main-widget';
import cityListJson from '../assets/city_codes.json';
import { Grid, Container } from 'semantic-ui-react'
import WeatherOtherCities from './weather-other_city-widget_pane';

class WeatherUI extends Component {

    constructor(props) {
        super(props);
        this.state = { cities: [] };
    }

    getCitiesFromJson() {
        return cityListJson;
    }

    componentDidMount() {
        var cityArr = [];

        this.getCitiesFromJson().List.map((city) => {
            cityArr = cityArr.concat(city);
        });
        this.setState({ cities: cityArr });
    }

    render() {
        return (
            <Container className="container" fluid>
                <Container>
                    <Grid>
                        <Grid.Row className="space"></Grid.Row>
                        <Grid.Row centered>
                            <WeatherMainWidget cities={this.state.cities} />
                        </Grid.Row>
                        <Grid.Row className="space2"></Grid.Row>
                        <Grid.Row centered >Other Cities</Grid.Row>
                        <Grid.Row className="other-cities-container">
                            <WeatherOtherCities cities={this.state.cities} />
                        </Grid.Row>
                    </Grid>
                </Container>

            </Container>
        );
    }
}

export default WeatherUI;
