import React, { Component } from 'react';
import './styles.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons'
import { faThermometerQuarter } from '@fortawesome/free-solid-svg-icons'
import { Label, Grid } from 'semantic-ui-react';

library.add(faMapMarkerAlt);
library.add(faThermometerQuarter);


class WeatherWidget extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <h3><FontAwesomeIcon icon="map-marker-alt" />    &nbsp;&nbsp;{this.props.cityName}</h3>
                <h2><FontAwesomeIcon icon="thermometer-quarter" />    &nbsp;&nbsp;{this.props.temp}<span>&#176;C</span></h2>
                    <Label className="label-row">
                        City Code:
                    <Label.Detail>{this.props.cityID}</Label.Detail>
                    </Label><br />
                    <Label className="label-row">
                        Description:
                    <Label.Detail>{this.props.description}</Label.Detail>
                    </Label>
            </div>
        );
    }
}

export default WeatherWidget;